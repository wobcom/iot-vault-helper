package config

type ChirpDeviceProfile struct {
	ClassBTimeout        int    `json:"classBTimeout" yaml:"class_b_timeout"`
	ClassCTimeout        int    `json:"classCTimeout" yaml:"class_c_timeout"`
	FactoryPresetFreqs   []int  `json:"factoryPresetFreqs" yaml:"factory_preset_freqs"`
	GeolocBufferTTL      int    `json:"geolocBufferTTL" yaml:"geoloc_buffer_ttl"`
	GeolocMinBufferSize  int    `json:"geolocMinBufferSize" yaml:"geoloc_min_buffer_size"`
	ID                   string `json:"id"`
	MacVersion           string `json:"macVersion" yaml:"mac_version"`
	MaxDutyCycle         int    `json:"maxDutyCycle" yaml:"max_duty_cycle"`
	MaxEIRP              int    `json:"maxEIRP" yaml:"max_eirp"`
	Name                 string `json:"name"`
	NetworkServerID      string `json:"networkServerID" yaml:"network_server_id"`
	OrganizationID       string `json:"organizationID" yaml:"organization_id"`
	PayloadCodec         string `json:"payloadCodec"`
	PayloadDecoderScript string `json:"payloadDecoderScript"`
	PayloadEncoderScript string `json:"payloadEncoderScript"`
	PingSlotDR           int    `json:"pingSlotDR" yaml:"ping_slot_dr"`
	PingSlotFreq         int    `json:"pingSlotFreq" yaml:"ping_slot_freq"`
	PingSlotPeriod       int    `json:"pingSlotPeriod" yaml:"ping_slot_period"`
	RegParamsRevision    string `json:"regParamsRevision" yaml:"reg_params_revision"`
	RfRegion             string `json:"rfRegion" yaml:"rf_region"`
	RxDROffset1          int    `json:"rxDROffset1" yaml:"rx_dr_offset_1"`
	RxDataRate2          int    `json:"rxDataRate2" yaml:"rx_data_rate_2"`
	RxDelay1             int    `json:"rxDelay1" yaml:"rx_delay_1"`
	RxFreq2              int    `json:"rxFreq2" yaml:"rx_freq_2"`
	Supports32BitFCnt    bool   `json:"supports32BitFCnt" yaml:"supports_32_bit_f_cnt"`
	SupportsClassB       bool   `json:"supportsClassB" yaml:"supports_class_b"`
	SupportsClassC       bool   `json:"supportsClassC" yaml:"supports_class_c"`
	SupportsJoin         bool   `json:"supportsJoin" yaml:"supports_join"`
	Tags                 struct {
	} `json:"tags"`
}

type Device struct {
	DevEUI        string `yaml:"dev_eui"`
	ApplicationID string `yaml:"application_id"`
	Name          string `yaml:"name"`
	Description   string `yaml:"description"`
	Key           string `yaml:"key"`
	Productive    *bool  `yaml:"productive"`
}

type Payload struct {
	Hex  string `yaml:"hex"`
	Port int    `yaml:"port"`
}

type Config struct {
	Name          string             `yaml:"name"`
	Schema        string             `yaml:"schema"`
	ConfigPayload []Payload          `yaml:"config_payload"`
	Chirpstack    ChirpDeviceProfile `yaml:"chirpstack"`
	Devices       []Device           `yaml:"devices"`
}
