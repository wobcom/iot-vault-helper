// nolint: goerr113
package config

import (
	"fmt"
	"strings"

	yamlToJSON "github.com/ghodss/yaml"
	"github.com/xeipuuv/gojsonschema"
)

// Valid validates a config file with a json schema.
func Valid(config, schema []byte) error {
	cJSON, err := yamlToJSON.YAMLToJSON(config)
	if err != nil {
		return err
	}

	doc := gojsonschema.NewBytesLoader(cJSON)
	scma := gojsonschema.NewBytesLoader(schema)

	result, err := gojsonschema.Validate(scma, doc)
	if err != nil {
		return err
	}

	if !result.Valid() {
		var errs []string
		errs = append(errs, fmt.Sprintln("Input is invalid, following errors found:"))

		for _, desc := range result.Errors() {
			errs = append(errs, fmt.Sprintf("- %s", desc))
		}

		return fmt.Errorf(strings.Join(errs, " "))
	}

	return nil
}
