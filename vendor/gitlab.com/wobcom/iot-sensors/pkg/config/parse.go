package config

import (
	"fmt"
	"io/ioutil"
	"strings"

	vault "github.com/sosedoff/ansible-vault-go"
	"gopkg.in/yaml.v2"
)

func (c *Config) Parse(configBytes []byte, decoderPath, vaultPass string) error {
	if err := yaml.Unmarshal(configBytes, c); err != nil {
		return err
	}

	// Read decoder.js.
	d, err := ioutil.ReadFile(decoderPath)
	if err != nil {
		return err
	}

	// Adds some static chirpstack attributes.
	c.Chirpstack.Name = c.Name
	c.Chirpstack.PayloadDecoderScript = strings.TrimSuffix(string(d), "\n")
	c.Chirpstack.PayloadCodec = "CUSTOM_JS"
	c.Chirpstack.OrganizationID = "1"
	c.Chirpstack.NetworkServerID = "4"

	prodTrue := true

	for j, i := range c.Devices {
		c.Devices[j].DevEUI = strings.ToLower(i.DevEUI)

		if i.Productive == nil {
			c.Devices[j].Productive = &prodTrue
		}

		if !*c.Devices[j].Productive {
			c.Devices[j].Name = fmt.Sprintf("[DEV] %s", i.Name)
		}

		if i.Key != "" {
			sec, err := vault.Decrypt(i.Key, vaultPass)
			if err != nil {
				return err
			}

			c.Devices[j].Key = sec
		}
	}

	return nil
}
