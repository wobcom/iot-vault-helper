# github.com/fatih/color v1.7.0
github.com/fatih/color
# github.com/ghodss/yaml v1.0.0
github.com/ghodss/yaml
# github.com/goccy/go-yaml v1.8.2
## explicit
github.com/goccy/go-yaml
github.com/goccy/go-yaml/ast
github.com/goccy/go-yaml/internal/errors
github.com/goccy/go-yaml/lexer
github.com/goccy/go-yaml/parser
github.com/goccy/go-yaml/printer
github.com/goccy/go-yaml/scanner
github.com/goccy/go-yaml/token
# github.com/inconshreveable/mousetrap v1.0.0
github.com/inconshreveable/mousetrap
# github.com/mattn/go-colorable v0.1.4
github.com/mattn/go-colorable
# github.com/mattn/go-isatty v0.0.10
github.com/mattn/go-isatty
# github.com/rs/zerolog v1.20.0
## explicit
github.com/rs/zerolog
github.com/rs/zerolog/internal/cbor
github.com/rs/zerolog/internal/json
github.com/rs/zerolog/log
# github.com/sosedoff/ansible-vault-go v0.0.0-20181205202858-ab5632c40bf5
## explicit
github.com/sosedoff/ansible-vault-go
# github.com/spf13/cobra v1.0.0
## explicit
github.com/spf13/cobra
# github.com/spf13/pflag v1.0.3
github.com/spf13/pflag
# github.com/xeipuuv/gojsonpointer v0.0.0-20180127040702-4e3ac2762d5f
github.com/xeipuuv/gojsonpointer
# github.com/xeipuuv/gojsonreference v0.0.0-20180127040603-bd5ef7bd5415
github.com/xeipuuv/gojsonreference
# github.com/xeipuuv/gojsonschema v1.2.0
github.com/xeipuuv/gojsonschema
# gitlab.com/wobcom/iot-sensors v0.0.0-20200925085906-5923d3d6daa5
## explicit
gitlab.com/wobcom/iot-sensors/pkg/config
# golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
golang.org/x/crypto/pbkdf2
# golang.org/x/sys v0.0.0-20191010194322-b09406accb47
golang.org/x/sys/unix
# golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543
golang.org/x/xerrors
golang.org/x/xerrors/internal
# gopkg.in/yaml.v2 v2.2.2
gopkg.in/yaml.v2
