module gitlab.com/wobcom/iot-vault-helper

go 1.15

require (
	github.com/goccy/go-yaml v1.8.2
	github.com/rs/zerolog v1.20.0
	github.com/sosedoff/ansible-vault-go v0.0.0-20181205202858-ab5632c40bf5
	github.com/spf13/cobra v1.0.0
	gitlab.com/wobcom/iot-sensors v0.0.0-20200925085906-5923d3d6daa5
)
