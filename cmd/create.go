// nolint: gochecknoglobals, gochecknoinits
package cmd

import (
	"fmt"

	"github.com/rs/zerolog/log"
	vault "github.com/sosedoff/ansible-vault-go"
	"github.com/spf13/cobra"
)

var secret string

func init() {
	rootCmd.AddCommand(createCmd)
	createCmd.Flags().StringVarP(&secret, "secret", "s", "", "secret to create vault for")

	if err := createCmd.MarkFlagRequired("secret"); err != nil {
		log.Fatal().Msg(err.Error())
	}
}

var createCmd = &cobra.Command{
	Use:   "create",
	Short: "Creates crypted secret for DeviceProfile config",
	Run: func(cmd *cobra.Command, args []string) {
		str, err := vault.Encrypt(secret, vaultKey)
		if err != nil {
			log.Fatal().Msg(err.Error())
		}
		fmt.Print(str)
	},
}
