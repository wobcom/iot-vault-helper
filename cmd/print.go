// nolint: gochecknoglobals, gochecknoinits
package cmd

import (
	"bytes"
	"io/ioutil"

	"github.com/goccy/go-yaml"
	"github.com/rs/zerolog/log"
	vault "github.com/sosedoff/ansible-vault-go"
	"github.com/spf13/cobra"
	iotConfig "gitlab.com/wobcom/iot-sensors/pkg/config"
)

var cfgFile string

func init() {
	rootCmd.AddCommand(printCmd)
	printCmd.Flags().StringVarP(&cfgFile, "config", "c", "", "config file")

	if err := printCmd.MarkFlagRequired("config"); err != nil {
		log.Fatal().Msg(err.Error())
	}
}

var printCmd = &cobra.Command{
	Use:   "print",
	Short: "Print device keys for a DeviceProfile",
	Run: func(cmd *cobra.Command, args []string) {
		cfg, err := ioutil.ReadFile(cfgFile)
		if err != nil {
			log.Fatal().Msg(err.Error())
		}

		var devs []iotConfig.Device

		path, err := yaml.PathString("$.devices[*]")
		if err != nil {
			log.Fatal().Msg(err.Error())
		}
		if err := path.Read(bytes.NewReader(cfg), &devs); err != nil {
			log.Fatal().Msg(err.Error())
		}

		keysFound := 0
		for _, d := range devs {
			if d.Key != "" {
				key, err := vault.Decrypt(d.Key, vaultKey)
				if err != nil {
					log.Fatal().Msg(err.Error())
				}
				log.Info().Msgf("%s (%s) -> %s", d.Name, d.DevEUI, key)
				keysFound++
			}
		}
		if keysFound == 0 {
			log.Info().Msg("no keys found")
		}
	},
}
