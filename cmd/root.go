// nolint: gochecknoglobals, gochecknoinits
package cmd

import (
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
)

// Flags.
var (
	vaultKey string
)

func init() {
	rootCmd.PersistentFlags().StringVarP(&vaultKey, "vault-key", "k", "", "vault key")
}

// root CMD.
var rootCmd = &cobra.Command{
	Use:   "iot-vault-helper",
	Short: "iot-vault-helper helps with handling iot config vault handling",
}

// Entrypoint.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal().Msg(err.Error())
	}
}
